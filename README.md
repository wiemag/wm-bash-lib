# wm-bash-lib

Simple bash scripts that could be sourced by other scripts, or just copied into them.

SCRIPT FILES

- wmtools_abortf.sh  -  run a function or print a message and exit with an exit code
- wmtools_dependencies.sh  -  check if dependencies are installed
- wmtools_free_days.sh  -  weekends and holidays:  check if a day is free
- wmtools_transfer_permissions.sh
- wmtools_yes-no.sh

INSTALLATION

These are regular (and simple) bash scripts that do not require special installation.

DEPENDENCIES

coreutils, and similar tools may be necessary, but they will have been already on your system.
