# Bash function library
# by wm, 2020-07-16
#
# abortf() - print message and exit, or run a function and exit
# compute() - a simple wrapper for the 'bc' command (calculator)
# chk_color() - check if the parameter is a color 'number number number'
# chk_deps() - check if dependencies are installed

# Bash source builder
# lets you source wmtools functions needed in your script.

function wmtools_souce_builder(){
:

}


# The double quots below is a trick to make sure there's a string
# on the left, as command 'type' does not output anythinag
# in case of an error.
# If 1st arg is a function, run it. If not, print it.
function abortf(){
	[ $(type -t "$1")"" == 'function' ] && "$1" || echo -e "$1"
	exit "${2-0}"
}

# Simple wrapper for the 'bc' calculator; No dependency verification.
function compute(){
	echo "scale=15; $1"|bc
}

# Check if the argument is a color defined as three decimal numbers
# (with a decimal point or not): 'number number number'
function chk_color(){
local col=($1) i msg="Wrong color definition.\n"
  (( ${#col[@]} == 3 )) || {
	msg+="Use three numbers [0..1] inside single or doube quotes."
 	abortf "$msg" $2;
  }
  for ((i=0; i<3; i++));
  do
	[[ ${col[$i]} =~ ^[0-9]*([.][0-9]+)?$ ]] ||
	  abortf "${msg}${col[$i]} is nor a number." $2
  done
}

# Check if dependencies are istalled.
function chk_deps(){
local deps=($@) i 
  for ((i=0; i<${#deps[@]}; i++))
  do
	hash ${deps[$i]} 2>/dev/null ||
	 abortf "${deps[$i]} is not installed."
  done
}
