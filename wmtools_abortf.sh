# If $1 is a function, run it. If a string, print it.
# Then abort the script with exit code $2.
function abortf() {
	[ $(type -t "${2:-}")"" != 'function' ] && echo -e "$2" >&2 || "$2"
    exit "${1-0}"
}

# The function could be a help-message function.
# Or a function that cleans up temporary files.
