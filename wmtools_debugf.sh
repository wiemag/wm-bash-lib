debugf() {
# Run:  debugf [comment] [a_reference_or_label]
local func_name="${FUNCNAME[@]}"
local line=${BASH_LINENO[*]}
	[[ -z "${2:-}" ]] && func_name+=" " || func_name=': '
	echo -e "[${func_name#debugf }${2:-${line% 0}}] ${1:-}" >&2;
}
