#!/bin/bash -
#===============================================================================
#          FILE: wmtools_dependencies.sh
#         USAGE: Source this file into a bash script.
#   DESCRIPTION: A library of simple functions for bash scripts.
#        AUTHOR: Wiesław Magusiak
#      REVISION: 0.12
#===============================================================================

# Dependency check.
# $1 - command to check
# $2 - missing dep / package to install
function install_chk(){
  hash "$1" 2>/dev/null || {
    echo Install $((($# > 1)) && echo $2 || echo $1)
    return 1
  }
}

function chk_list_of_deps(){
  for dep in "$@"; do
    hash "$dep" 2>/dev/null || {
      echo -e "\nMissing dependency: \e[1m${1}\e[0m needed."
      return 1
    }
  done
}

# Check if dependencies are istalled.
# The function argument is the NAME of an associative array,
# where the keys are commands to check and the values are packages to install
# e.g. dependcies=( [pdftohtml]=poppler [awk]=gawk [sed]=sed )
function chk_deps(){
local -n deps_ref=$1 # -n = name reference; $1  is the name of Associative arr.
local missing=0
  for dep in "${!deps_ref[@]}"
  do
    hash $dep 2>/dev/null || {
      missing=1
      echo "${deps_ref[$dep]} is not installed."
    }
  done
  return $missing
}
