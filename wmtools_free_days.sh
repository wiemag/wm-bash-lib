# Functions:
#   IsWeekEnd(YYYY-MM-DD): 0 (yes) or ≠0 (not) / boolean
#   Easter(year): YYYY-MM-DD
#   IsHoliday(YYYY-MM-DD): 0 (yes) or ≠0 (not) / boolean
#   IsFreeDay(YYYY-MM-DD): 0 (yes) or ≠0 (not) / boolean
# Array, month-day (MM-DD format)
#   FixedHolidaysArray

FixedHolidaysArray=(01-01 01-06 05-01 05-03 08-15 11-01 11-11 12-25 12-26)

function IsWeekEnd(){
	(( $(date -d "$1" +%u) > 5 ))
}

function Easter(){
# Easter(year): YYYY-MM-DD
# http://www.grahamhague.com/softwareeaster.shtml
# http://www.algorytm.org/przetwarzanie-dat/wyznaczanie-daty-wielkanocy-metoda-meeusa-jonesa-butchera.html
local a b c d e f g h i k L m month day Y=$1
	a=$((Y % 19)); b=$((Y / 100)); c=$((Y % 100))
	d=$((b / 4)); e=$((b % 4)); f=$(((b + 8) / 25))
	g=$(((b - f + 1) / 3))
	h=$(((19 * a + b - d - g + 15) % 30))
	i=$((c / 4))
	k=$((c % 4))
	L=$(((32 + 2 * e + 2 * i - h - k) % 7))
	m=$(((a + 11 * h + 22 * L) / 451))
	month=$(((h + L - 7 * m + 114) / 31))
	day=$((((h + L - 7 * m + 114) % 31) + 1))
	printf "%d-%02d-%02d" ${Y} ${month} ${day}
}


function IsHoliday(){
  local day=${1#*-}
  local EasterDate=$(Easter "${1%%-*}")
  local EasterDay EasterMonday PentecostDay CorpusChristiDay
  local declare hoDayArray=(${FixedHolidaysArray[@]})
  local hoDay
  EasterDay=$(date +%m-%d -d "$EasterDate")
  EasterMonday=$(date +%m-%d -d "$EasterDate + 1 day")
  PentecostDay=$(date +%m-%d -d "$EasterDate + 49 days")
  CorpusChristiDay=$(date +%m-%d -d "$EasterDate + 60 days")
  hoDayArray+=("$EasterDay"	"$EasterMonday" "$PentecostDay" "$CorpusChristiDay")
  for hoDay in ${hoDayArray[@]}
  do
  	[[ "$day" == "$hoDay" ]] && return 0
  done
  return 1
}

function IsFreeDay(){
  local day="$1"
  IsWeekEnd "$day" || IsHoliday "$day"
}
