# Check if command $1 is available, and if not informs which package to install.
function install_chk(){
	hash "$1" 2>/dev/null || {
		echo Install $((($# > 1)) && echo $2 || echo $1)
		return 1
	}
}

# Suggestion:  declare dependencies as an associative array
# and use its keys/indeces as $1 and its values as $2 in a loop.
# E.g.
# abort=0
# for pak in "${!dependencies[@]}"
# do
# 	install_chk $pak ${dependencies[$pak]} || abort=1
# done
# ((abort)) && exit 1;
