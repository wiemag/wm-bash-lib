permissions_transfer(){
local infile, outfile, permissions
infile="${1-}"
outfile="${2-}
	[[ -f "$infile" && -f "$outfile" ]] && {
		permissions=$(stat -L -c "%a" "$infile") &&
		chmod "$permissions" "$outfile";
	}
