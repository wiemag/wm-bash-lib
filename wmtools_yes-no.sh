function yesno(){
	# Run: yesno [Hint [New_prompt_string]]
	# Hint makes Y or N the default value (for ENTER to confirm).
	# If Hint=(Y/n), [nN] has to be pressed to return 1
	# If Hint=(y/N), [yY] has to be pressed to return 0
	local promptstr ans hint='(Y/n)' chrcode
	ans="${1:-Y}"
	((${#1})) && shift
	ans="${ans:0:1}"; ans=${ans^} 	# uppercase
	[[ ${ans} = 'N' ]] && hint='(y/N)' || ans='Y'
	promptstr="${1:-Are you sure?  $hint }"
	read -p "$promptstr" -n 1 -r -s
	echo
	# If ENTER pressed, $charcode length is zero.
	chrcode=$(od -An -t x1 <<< "$REPLY" |sed 's/ 0a//')
	(( ${#chrcode} )) || REPLY=$ans
	if [ $ans = 'Y' ]
	then
		( test "${REPLY^}" != 'N' ) && return 0 || return 1
	else # ans=N
		( test "${REPLY^}" != 'Y' ) && return 1 || return 0
	fi
}

function are_you_sure(){
  read -r -p "Are you sure? [y/N]  "
  [[ "${REPLY,,}" =~ ^(yes|y) ]] && return 0 || return 1
}
 #read -r -p "Are you sure? [Y/n]" response
 #response=${response,,} # tolower
 #if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then
 #   your-action-here
 #fi
